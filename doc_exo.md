# EXO SCRAPING WEB PARANORMAL TO CREATE A DATABASE OF "GHOSTS"
[Site link](https://www.paranormaldatabase.com/)

## WEB analysis
Duplicates:

* Home > Reports: Places > Shops
[Link](https://www.paranormaldatabase.com/reports/shopping.php)

Naked Woman

Location: Brighton (Sussex) - Junk Shop, Upper Rock Gardens (no longer exists)
Type: Haunting Manifestation
Date / Time: 1950s
Further Comments: This naked, disfigured female phantom was said to have been murdered in the building's basement by her insane husband. A private house in the same area is home to the benign presence of an elderly woman.

* Home > South East England > Sussex
[Link](https://www.paranormaldatabase.com/sussex/suspages/sussdata.php?pageNum_paradata=4&totalRows_paradata=445)
Naked Woman

Location: Brighton - Junk Shop, Upper Rock Gardens (no longer exists)
Type: Haunting Manifestation
Date / Time: 1950s
Further Comments: This naked, disfigured female phantom was said to have been murdered in the building's basement by her insane husband. A private house in the same area is home to the benign presence of an elderly woman.

* Home > South East England > Brighton
[Link](https://www.paranormaldatabase.com/hotspots/brighton.php?pageNum_paradata=2&totalRows_paradata=146)
Naked Woman

Location: Brighton - Junk Shop, Upper Rock Gardens (no longer exists)
Type: Haunting Manifestation
Date / Time: 1950s
Further Comments: This naked, disfigured female phantom was said to have been murdered in the building's basement by her insane husband. A private house in the same area is home to the benign presence of an elderly woman.

## Models
MCD
![Alt text](MCD.png)
An analysis of the website reveals that the cases from Reports and Calendar, as well as from Places of Note, in those cases, embedded within the regional subcategories, are repeated.

Tree Models:
There are 3 trees containing cases, a geographical by regions, another associated to the calendar and another one related to reports (cases by place, type...)
![Alt text](tree_geo.png)
![Alt text](report_calendar_tree.png)

MPD
[Go to link](https://drawsql.app/teams/lola-4/diagrams/ghost-db)
![Alt text](ghost_db.png)

## Work-flow chart

![Alt text](workflow.png)

## DB
```sql
select * from elements;
+----+--------------------------+-----------+------------+
| id | name                     | id_parent | category   |
+----+--------------------------+-----------+------------+
|  1 | South East England       |      NULL | geographic |
|  2 | South West England       |      NULL | geographic |
|  3 | Greater London           |      NULL | geographic |
|  4 | East of England          |      NULL | geographic |
|  5 | East Midlands            |      NULL | geographic |
|  6 | West Midlands            |      NULL | geographic |
|  7 | Northern Ireland         |      NULL | geographic |
|  8 | Republic of Ireland      |      NULL | geographic |
|  9 | Other Regions            |      NULL | geographic |
| 10 | North West England       |      NULL | geographic |
| 11 | North East and Yorkshire |      NULL | geographic |
| 12 | Scotland                 |      NULL | geographic |
| 13 | Wales                    |      NULL | geographic |
| 14 | Reports: Places          |      NULL | report     |
| 15 | Reports: Browse Type     |      NULL | report     |
| 16 | Reports: People          |      NULL | report     |
+----+--------------------------+-----------+------------+
16 rows in set (0.00 sec)

```
List of categories (link and name):

```sql
/regions/southeast.html
South East England
/regions/southwest.html
South West England
/regions/greaterlondon.html
Greater London
/regions/eastengland.html
East of England
/regions/eastmidlands.html
East Midlands
/regions/westmidlands.html
West Midlands
/regions/northernireland.html
Northern Ireland
/regions/ireland.html
Republic of Ireland
/regions/otherregions.html
Other Regions
/regions/northwest.html
North West England
/regions/northeastandyorks.html
North East and Yorkshire
/regions/scotland.html
Scotland
/regions/wales.html
Wales
/reports/reports.htm
Reports: Places
/calendar/Pages/calendar.html
Calendar
/reports/reports-type.html
Reports: Browse Type
/reports/reports-people.html
Reports: People```

```sql
/calendar/Pages/calendar.html
/calendar/Pages/march.php
/calendar/Pages/apr.php
/calendar/Pages/may.php
/calendar/Pages/jun.php
/calendar/Pages/jul.php
/calendar/Pages/aug.php
/calendar/Pages/sep.php
/calendar/Pages/oct.php
/calendar/Pages/nov.php
/calendar/Pages/jan.php
/calendar/Pages/feb.php
/calendar/Pages/dec.php
/calendar/Pages/weather.php```


