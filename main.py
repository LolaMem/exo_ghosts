from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector

connection =  mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    database="ghosts"
)

def get_page_content(url):
    return urlopen("https://www.paranormaldatabase.com"+url).read()

# get content home
html_home = get_page_content("/index.html")

# get category home
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

cursor = connection.cursor()
list_names_cat_id =[]

# Loop through 1st level of the tree (content of /index.html website)
for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()
    #print(cat)  /regions/southeast.html

    # Identify the elements into cat geographic (contains regions):
    if cat.find("region")>0:
        cursor.execute("SELECT id FROM elements WHERE name =%s;",(titre_cat,))
        result = cursor.fetchone()
        cat_type = "geographic"

    # If not in the DB then insert into the table elements with category geographic
        if not result:
            cursor.execute("INSERT INTO elements (name, id_parent,category) VALUES (%s, NULL, 'geographic');",(titre_cat,))
            connection.commit()
            id = cursor.lastrowid
        else:
            id = result[0]   

    # Identify the elements into cat report (contains report):
    elif cat.find("report")>0:
            cursor.execute("SELECT id FROM elements WHERE name =%s;",(titre_cat,))
            result = cursor.fetchone()
            cat_type = "report"

            if not result:
                cursor.execute("INSERT INTO elements (name, id_parent,category) VALUES (%s, NULL, 'report');",(titre_cat,))
                connection.commit()
                id = cursor.lastrowid
            else:
                id = result[0]
    elif cat.find("calendar")>0:
        pass     
        # If not category geographic, report or calendar then go to next in the loop
    else:
         continue

    # # Add to the list dictionnaries with the id for the DB and the name of the element
    # list_names_cat_id.append({"id": id, "name": titre_cat, "category": cat_type})
    print(cat)
    #print(titre_cat)   
    
    list_sous_cat = []

    liste_quarter = soup.find_all(class_="w3-quarter")
    for quarter in liste_quarter:
        lien = quarter.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for half in liste_halfs:
        lien = half.find('a')
        url= lien.get('href')
        list_sous_cat.append(url)
  

    cursor2= connection.cursor()
    for ss_cat in list_sous_cat:
        # Error with London Underground, link: "../reports/underground.php"
        ss_cat= ss_cat.replace("..", "")
        # Error with reports type, animal, link:  "animal.php"
        if not ss_cat.startswith("/"):
            ss_cat = "/"+ss_cat
        else:
            print(ss_cat)
            html_sous_cat = get_page_content(ss_cat)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")
            # prendre plutot le titre h3 de la page des cas
            titre_ss_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
            titre_ss_cat.find("a").extract()
            titre_ss_cat.find("a").extract()
            titre_ss_cat = titre_ss_cat.getText()
            titre_ss_cat = titre_ss_cat.replace("> ", "")
            titre_ss_cat = titre_ss_cat.strip()

            cursor2.execute("SELECT id FROM elements WHERE name =%s;",(titre_ss_cat,))
            result = cursor2.fetchone()

            if not result:
                cursor2.execute("INSERT INTO elements (name, id_parent,category) VALUES (%s, %s,%s);",(titre_ss_cat,id, cat_type))
                connection.commit()
                id_ss_cat = cursor2.lastrowid
            else:
                id_ss_cat = result[0]

            #print(titre_ss_cat)
            
            # refaire tout pour chaque page de la pagination
                
            # Insert the cases in the DB:
            qs = "?"
            while(qs):
                html_sous_cat = get_page_content(ss_cat+qs)
                soup = BeautifulSoup(html_sous_cat, features="html.parser")

                #list_cas = soup.select("div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")
                list_cas = soup.select("div.w3-panel > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")
                n=0
                for cas in list_cas:
                    # parser le cas

                    # title varchar(255),
                    titre_cas = cas.select_one("h4").get_text()

                    # recup le fameux P
                    p = cas.select_one("p:nth-last-child(1)")
                    p_text = p.get_text().split("\n")
                    # location varchar(255),
                    loc = p_text[0].replace("Location: ", "")
                    # type varchar(255),
                    type = p_text[1].replace("Type: ", "")
                    # date varchar(255),
                    date_cas = p_text[2].replace("Date / Time: ", "")
                    # comments varchar(255),
                    comments = p_text[3].replace("Further Comments: ", "")
                    n =n+1
                    
                    # if titre_cat == "Calendar":
                    #     pass
                    #     # month dans la cat calendar
                    #     # weather dans la cat calencar

                    # insert into
                    #print(n, titre_cas, loc, type)
                    cursor2.execute("INSERT INTO cases (title, location, type, date, description) VALUES (%s, %s,%s, %s,%s);",(titre_cas,loc, type, date_cas, comments))
                    connection.commit()

                    # next
                    nav_btn = soup.select(".w3-quarter.w3-container h5")
                    qs = None
                    for btn in nav_btn:
                        if btn.get_text().find('next') > 0:
                            qs = btn.select_one("a").get("href")
            
        


cursor.close()
cursor2.close()
connection.close()