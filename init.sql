DROP DATABASE IF EXISTS ghosts;
CREATE DATABASE ghosts;
Use ghosts;

CREATE TABLE `cases`(
    `id_case` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255) NOT NULL,
    `location` VARCHAR(255) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    `date` VARCHAR(255) NOT NULL,
    `description` text NOT NULL,
    `month` INT,
    `weather` TINYINT(1)
);
CREATE TABLE `elements`(
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `id_parent` INT,
    `category` ENUM('geographic','report') NOT NULL
);
CREATE TABLE `case_element`(
    `id_case` INT NOT NULL,
    `id_element` INT NOT NULL
);

ALTER TABLE
    `elements` ADD CONSTRAINT `elements_id_parent` FOREIGN KEY(`id_parent`) REFERENCES `elements`(`id`);
ALTER TABLE
    `case_element` ADD CONSTRAINT `case_element_id_element_foreign` FOREIGN KEY(`id_element`) REFERENCES `elements`(`id`);
ALTER TABLE
    `case_element` ADD CONSTRAINT `case_element_id_case_foreign` FOREIGN KEY(`id_case`) REFERENCES `cases`(`id_case`);


-- CREATE TABLE `case_element`(
--     `id_case` INT UNSIGNED NOT NULL AUTO_INCREMENT,
--     `id_element` INT NOT NULL
-- );